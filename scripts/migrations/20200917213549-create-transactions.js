module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.sequelize.transaction(async (transaction) => {
      await queryInterface.createTable('transactions', {
        id: {
          type: Sequelize.UUID,
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true,
        },
        user_id: {
          type: Sequelize.UUID,
          allowNull: false,
          references: {
            model: {
              tableName: 'users',
            },
            key: 'id',
          },
        },
        date: {
          type: Sequelize.DATE,
          allowNull: false,
        },
        amount: {
          type: Sequelize.BIGINT,
          allowNull: false,
        },
        description: {
          type: Sequelize.TEXT,
        },
        merchant_id: {
          type: Sequelize.UUID,
          allowNull: false,
          references: {
            model: {
              tableName: 'merchants',
            },
            key: 'id',
          },
        },
        created_at: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.fn('NOW'),
          allowNull: false,
        },
        updated_at: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.fn('NOW'),
          allowNull: false,
        },
      }, { transaction });

      await queryInterface.addIndex('transactions', ['user_id'], { transaction });
      await queryInterface.addIndex('transactions', ['date'], { transaction });
    });
  },

  async down(queryInterface) {
    await queryInterface.sequelize.transaction(async (transaction) => {
      await queryInterface.removeIndex('transactions', ['user_id'], { transaction });
      await queryInterface.removeIndex('transactions', ['date'], { transaction });
      await queryInterface.dropTable('transactions', { transaction });
    });
  },
};
