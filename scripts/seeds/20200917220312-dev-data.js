const uuid = require('uuid');

module.exports = {
  async up(queryInterface, Sequelize) {
    const users = [];
    const merchants = [];
    const txs = [];

    for (let i = 1; i <= 10000; i += 1) {
      const user = {
        id: uuid.v4(),
        first_name: 'Devon',
        last_name: `Testerson${i}`,
        created_at: Sequelize.fn('NOW'),
        updated_at: Sequelize.fn('NOW'),
      };

      users.push(user);

      const merchant = {
        id: uuid.v4(),
        display_name: `Test Merchant ${i}`,
        icon_url: 'https://picsum.photos/200',
        funny_gif_url: 'https://media.giphy.com/media/PqASLvTK3DRjq/giphy.gif',
        created_at: Sequelize.fn('NOW'),
        updated_at: Sequelize.fn('NOW'),
      };

      merchants.push(merchant);
    }

    for (let j = 0; j < 100000; j += 1) {
      txs.push({
        id: uuid.v4(),
        date: Sequelize.fn('NOW'),
        user_id: users[Math.floor(Math.random() * users.length)].id,
        amount: Math.floor(Math.random() * 1000),
        description: '#TEST',
        merchant_id: merchants[Math.floor(Math.random() * merchants.length)].id,
        created_at: Sequelize.fn('NOW'),
        updated_at: Sequelize.fn('NOW'),
      });
    }

    await queryInterface.sequelize.transaction(async (transaction) => {
      await queryInterface.bulkInsert('users', users, { transaction });
      await queryInterface.bulkInsert('merchants', merchants, { transaction });
      await queryInterface.bulkInsert('transactions', txs, { transaction });
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.sequelize.transaction(async (transaction) => {
      await queryInterface.bulkDelete('transactions', {
        description: { [Sequelize.Op.eq]: '#TEST' },
      },
      { transaction });

      await queryInterface.bulkDelete('users', {
        last_name: { [Sequelize.Op.like]: 'Testerson%' },
      },
      { transaction });

      await queryInterface.bulkDelete('merchants', {
        display_name: { [Sequelize.Op.like]: 'Test Merchant %' },
      },
      { transaction });
    });
  },
};
