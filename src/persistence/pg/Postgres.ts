import { Sequelize, Transaction as DBTransaction } from 'sequelize';
import defineTransactions, { Transaction } from './models/Transaction';
import defineMerchants, { Merchant } from './models/Merchant';
import defineUserMerchantSummary, { UserMerchantSummary } from './models/UserMerchantSummary';

interface Config {
  host: string;
  port: number;
  user: string;
  pass: string;
  db: string;
}

/**
 * Postgres is a persistent store backed by Postgres.
 */
export default class Postgres {
  private db: Sequelize;

  constructor(config: Config) {
    const { host, port } = config;

    this.db = new Sequelize(
      config.db,
      config.user,
      config.pass,
      {
        host, port, dialect: 'postgres', logging: false,
      },
    );

    defineMerchants(this.db);
    defineTransactions(this.db);
    defineUserMerchantSummary(this.db);

    Merchant.hasMany(Transaction);
    Transaction.belongsTo(Merchant, { as: 'merchant' });
  }

  /**
   * Test the db connection.
   */
  connect(): Promise<void> {
    return this.db.authenticate();
  }

  /**
   * Drop the current database connection.
   */
  disconnect(): Promise<void> {
    return this.db.close();
  }

  /**
   * Retrieve a summary of a user's spending.
   *
   * @param {string} userId - The user ID to look up.
   * @param {Date} from - The start of the date range.
   * @param {Date} to - The end of the date range.
   *
   * @returns {Promise<UserMerchantSummary[]>} The summary for each merchant spent at.
   */
  async getSummary(userId: string, from: Date, to: Date): Promise<UserMerchantSummary[]> {
    const viewName = `user_merchants_${from.getTime()}_${to.getTime()}`;

    // Create a view of each users total spend at each merchant, filtered by the
    // specified date range. Materialized views are only calculated on creating and
    // manual refresh, so this allows us to run the subqueries below without running
    // this one again.
    const createViewQuery = `
      CREATE MATERIALIZED VIEW IF NOT EXISTS ${viewName} AS 
      SELECT transactions.user_id, transactions.merchant_id,
        merchants.display_name, SUM(transactions.amount) AS total_spent
      FROM transactions JOIN merchants ON merchants.id = transactions.merchant_id 
      WHERE transactions.date >= ? AND transactions.date <= ? 
      GROUP BY merchants.display_name, transactions.merchant_id, transactions.user_id;
    `;

    // To calculate the percentage we take the number of users which spent less
    // than the current user and divide that by the number of all other users which
    // spent there. For example, if 12 out of 31 total users spent less than this user,
    // then we do 12/30 * 100 = 40%.
    //
    // We use NULLIF here to convert a possible 0 when there's only a single user,
    // which we can then COALESCE into a 1, to give us 100%.
    const userQuery = `
      SELECT
      display_name,
      total_spent, 
      COALESCE(
        (
          (SELECT COUNT(user_id) FROM ${viewName} WHERE merchant_id = m.merchant_id AND total_spent < m.total_spent) /
          (SELECT NULLIF(COUNT(user_id)::decimal - 1, 0) FROM ${viewName} WHERE merchant_id = m.merchant_id)
        ),
        1
      )::float * 100 as percent
    FROM ${viewName} as m
    WHERE user_id = ?;
    `;

    // We use a transaction here to prevent any concurrency issues
    // if the same view is created in a concurrent request. There's currently
    // no way to create temporary or session scoped materialized views so we
    // need to create, read and drop each time.
    const transaction: DBTransaction = await this.db.transaction();

    try {
      await this.db.query(createViewQuery, {
        raw: true,
        replacements: [from, to],
        transaction,
      });

      await this.db.query(`CREATE INDEX IF NOT EXISTS merchant_id_${viewName}_idx ON ${viewName}(merchant_id);`, {
        raw: true,
        transaction,
      });

      await this.db.query(`CREATE INDEX IF NOT EXISTS total_spent_${viewName}_idx ON ${viewName}(total_spent);`, {
        raw: true,
        transaction,
      });

      const userMerchantSummaries = await this.db.query(userQuery, {
        model: UserMerchantSummary,
        replacements: [userId],
        mapToModel: true,
        transaction,
      });

      await this.db.query(`DROP MATERIALIZED VIEW ${viewName};`, {
        raw: true,
        transaction,
      });

      await transaction.commit();

      return userMerchantSummaries;
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  }
}
