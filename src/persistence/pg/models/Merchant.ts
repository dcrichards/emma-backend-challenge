import {
  Optional, Model, Sequelize, DataTypes,
} from 'sequelize';

interface MerchantModel {
  id: string;
  displayName: string;
  iconUrl: string | null;
  funnyGifUrl: string | null;
}

interface OptionalAttrs extends Optional<MerchantModel, 'id' | 'iconUrl' | 'funnyGifUrl'> {}

export class Merchant extends Model<MerchantModel, OptionalAttrs> {
  public id!: string;

  public displayName!: string;

  public iconUrl!: string | null;

  public funnyGifUrl!: string | null;

  public readonly createdAt!: Date;

  public readonly updatedAt!: Date;
}

export default (sequelize: Sequelize) => Merchant.init({
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
  },
  displayName: {
    type: DataTypes.TEXT,
    allowNull: false,
  },
  iconUrl: {
    type: DataTypes.STRING,
  },
  funnyGifUrl: {
    type: DataTypes.STRING,
  },
}, {
  sequelize,
  tableName: 'merchants',
  underscored: true,
});
