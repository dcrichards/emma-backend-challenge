import {
  Optional, Model, Sequelize, DataTypes,
} from 'sequelize';

interface UserModel {
  id: string;
  firstName: string;
  lastName: string;
}

interface OptionalAttrs extends Optional<UserModel, 'id'> {}

export class User extends Model<UserModel, OptionalAttrs> {
  public id!: string;

  public firstName!: string;

  public lastName!: string;
}

export default (sequelize: Sequelize) => User.init({
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
  },
  firstName: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  lastName: {
    type: DataTypes.STRING,
    allowNull: false,
  },
}, {
  sequelize,
  tableName: 'users',
  underscored: true,
});
