import {
  Optional, Association, Model, Sequelize, DataTypes,
} from 'sequelize';
import { Merchant } from './Merchant';

interface TransactionModel {
  id: string;
  userId: string;
  date: Date;
  amount: number;
  description: string | null;
  merchantId: string;
}

interface OptionalAttrs extends Optional<TransactionModel, 'id' | 'description'> {}

export class Transaction extends Model<TransactionModel, OptionalAttrs> {
  public id!: string;

  public userId!: string;

  public date!: Date;

  public amount!: number;

  public description!: string | null;

  public merchantId!: string;

  public readonly createdAt!: Date;

  public readonly updatedAt!: Date;

  public readonly merchant?: Merchant;

  public static associations: {
    merchant: Association<Transaction, Merchant>;
  };
}

export default (sequelize: Sequelize) => Transaction.init({
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
  },
  userId: {
    type: DataTypes.UUID,
    allowNull: false,
    references: {
      model: 'user',
      key: 'id',
    },
  },
  date: {
    type: DataTypes.DATE,
    allowNull: false,
  },
  amount: {
    type: DataTypes.BIGINT,
    allowNull: false,
  },
  description: {
    type: DataTypes.STRING,
  },
  merchantId: {
    type: DataTypes.UUID,
    allowNull: false,
    references: {
      model: 'Merchant',
      key: 'id',
    },
  },
}, {
  sequelize,
  tableName: 'transactions',
  underscored: true,
});
