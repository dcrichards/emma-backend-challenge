import { Model, Sequelize, DataTypes } from 'sequelize';

interface UserMerchantSummaryModel {
  displayName: string;
  totalSpent: string;
  percent: number;
}

export class UserMerchantSummary extends Model<UserMerchantSummaryModel, UserMerchantSummaryModel> {
  public displayName!: string;

  public totalSpent!: string;

  public percent!: number;
}

export default (sequelize: Sequelize) => UserMerchantSummary.init({
  displayName: {
    type: DataTypes.TEXT,
  },
  totalSpent: {
    type: DataTypes.INTEGER,
  },
  percent: {
    type: DataTypes.FLOAT,
  },
}, {
  sequelize,
  underscored: true,
});
