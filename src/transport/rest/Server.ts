/* eslint class-methods-use-this: 0 */

import { Server as HttpServer } from 'http';
import Koa, { Context, Next } from 'koa';
import Router from 'koa-router';
import parseDate from 'date-fns/parse';
import isValidDate from 'date-fns/isValid';

type LogFn = (msg: string, ...args: any[]) => void;

interface Logger {
  debug: LogFn;
  info: LogFn;
  error: LogFn;
}

interface UserSummary {
  displayName: string;
  totalSpent: string;
  percent: number;
}

export interface UserSpendingService {
  getSummary(userId: string, from: Date, to: Date): Promise<UserSummary[]>
}

interface Config {
  userSpendingService: UserSpendingService;
  logger: Logger;
}

/**
 * Server is a http.Server compatible HTTP Server for serving RESTful endpoints.
 */
export default class Server extends HttpServer {
  private userSpendingService: UserSpendingService;

  private log: Logger;

  constructor(config: Config) {
    super();

    this.log = config.logger;
    this.userSpendingService = config.userSpendingService;

    const router = new Router();
    router.get('/alive', this.healthcheck.bind(this));
    router.get('/user/:userId/spending/summary', this.spendingSummary.bind(this));

    const app = new Koa();
    app.use(this.logRequests.bind(this));
    app.use(this.handleError);
    app.use(router.routes()).use(router.allowedMethods());
    app.use(this.notFound);

    app.on('error', (err: Error) => {
      this.log.error(err.message);
    });

    this.on('request', app.callback());
  }

  private async healthcheck(ctx: Context): Promise<void> {
    ctx.body = { alive: true };
  }

  private async spendingSummary(ctx: Context): Promise<void> {
    const { userId } = ctx.params;
    const { from, to } = ctx.query;

    if (!userId) {
      ctx.throw(400, 'Invalid user');
    }

    if (!from) {
      ctx.throw(400, 'from parameter is required');
    }
    if (!to) {
      ctx.throw(400, 'to parameter is required');
    }

    const fromDate = parseDate(from, 'yyyy-MM-dd', new Date());
    const toDate = parseDate(to, 'yyyy-MM-dd', new Date());

    if (!isValidDate(fromDate)) {
      ctx.throw(400, 'from date should be in the format YYYY-MM-DD');
    }

    if (!isValidDate(toDate)) {
      ctx.throw(400, 'to date should be in the format YYYY-MM-DD');
    }

    if (fromDate > toDate) {
      ctx.throw(400, 'to date must be after from date');
    }

    const data = await this.userSpendingService.getSummary(userId, fromDate, toDate);

    ctx.body = {
      merchants: data.map((d) => ({
        merchant: d.displayName,
        percentile: Number(d.percent.toFixed(2)),
      })),
    };
  }

  private async notFound(ctx: Context, next: Next) {
    await next();
    if (ctx.status === 404 || ctx.status === 405) {
      ctx.throw(404, 'Not found');
    }
  }

  private async handleError(ctx: Context, next: Next): Promise<void> {
    try {
      await next();
    } catch (err) {
      const error = err.expose ? err.message : 'An error occured';
      ctx.status = err.status || 500;
      ctx.body = { error };
      ctx.app.emit('error', err, ctx);
    }
  }

  private async logRequests(ctx: Context, next: Next): Promise<void> {
    try {
      this.log.info(`request: ${ctx.method} ${ctx.originalUrl}`);
      await next();
    } catch (err) {
      this.log.error(`${err.status} ${err.message}`);
      throw err;
    } finally {
      this.log.info(`response: ${ctx.status} ${ctx.method} ${ctx.originalUrl}`);
    }
  }
}
