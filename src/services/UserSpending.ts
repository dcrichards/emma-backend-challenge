interface UserSummary {
  displayName: string;
  totalSpent: string;
  percent: number;
}

interface Persistence {
  connect(): Promise<void>;

  disconnect(): Promise<void>;

  getSummary(userId: string, from: Date, to: Date): Promise<UserSummary[]>;
}

/**
 * A service to handle any business logic between the persistence
 * and the handler.
 */
export default class UserSpending {
  private db: Persistence;

  constructor(persistence: Persistence) {
    this.db = persistence;
  }

  async getSummary(userId: string, from: Date, to: Date): Promise<UserSummary[]> {
    return this.db.getSummary(userId, from, to);
  }
}
