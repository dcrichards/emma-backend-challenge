import { config } from 'dotenv';
import pino from 'pino';
import Postgres from './persistence/pg/Postgres';
import Server from './transport/rest/Server';
import UserSpending from './services/UserSpending';

config();

const {
  HOST,
  PORT,
  LOG_LEVEL,
  POSTGRES_HOST,
  POSTGRES_PORT,
  POSTGRES_USER,
  POSTGRES_PASSWORD,
  POSTGRES_DB,
} = process.env;

const envError = 'Environment: You must specify';

if (!HOST) {
  throw new Error(`${envError} HOST`);
}
if (!PORT) {
  throw new Error(`${envError} PORT`);
}
if (!POSTGRES_HOST) {
  throw new Error(`${envError} POSTGRES_HOST`);
}
if (!POSTGRES_PORT) {
  throw new Error(`${envError} POSTGRES_PORT`);
}
if (!POSTGRES_DB) {
  throw new Error(`${envError} POSTGRES_DB`);
}
if (!POSTGRES_USER) {
  throw new Error(`${envError} POSTGRES_USER`);
}
if (!POSTGRES_PASSWORD) {
  throw new Error(`${envError} POSTGRES_PASSWORD`);
}

const level = LOG_LEVEL || 'info';
const logger = pino({ level });

const db = new Postgres({
  host: POSTGRES_HOST,
  port: Number.parseInt(POSTGRES_PORT, 10),
  user: POSTGRES_USER,
  pass: POSTGRES_PASSWORD,
  db: POSTGRES_DB,
});

const userSpendingService = new UserSpending(db);

const server = new Server({
  userSpendingService,
  logger,
});

server.listen(Number.parseInt(PORT, 10), HOST);
logger.info(`🐻 Serving on ${HOST}:${PORT}`);
