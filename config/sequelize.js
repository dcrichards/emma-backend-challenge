const dotenv = require('dotenv');

dotenv.config();

const {
  POSTGRES_HOST,
  POSTGRES_PORT,
  POSTGRES_USER,
  POSTGRES_PASSWORD,
  POSTGRES_DB,
} = process.env;

const config = {
  username: POSTGRES_USER,
  password: POSTGRES_PASSWORD,
  database: POSTGRES_DB,
  host: POSTGRES_HOST,
  port: Number.parseInt(POSTGRES_PORT, 10),
  dialect: 'postgres',
};

module.exports = {
  development: {
    ...config,
  },
  test: {
    ...config,
    database: `${POSTGRES_DB}_test`,
  },
};
