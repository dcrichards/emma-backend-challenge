import { expect } from 'chai';
import { rejects } from 'assert';
import add from 'date-fns/add';
import Postgres from '../../../src/persistence/pg/Postgres';
import {
  getDb, Merchant, Transaction, User,
} from './utils';

const {
  NODE_ENV,
  POSTGRES_HOST,
  POSTGRES_PORT,
  POSTGRES_USER,
  POSTGRES_PASSWORD,
  POSTGRES_DB,
} = process.env;

if (NODE_ENV !== 'test') {
  throw new Error('NODE_ENV is not set to \'test\'. Database tests may only run in test env.');
}

const config = {
  host: POSTGRES_HOST!,
  port: Number.parseInt(POSTGRES_PORT!, 10),
  user: POSTGRES_USER!,
  pass: POSTGRES_PASSWORD!,
  db: `${POSTGRES_DB!}_test`,
};

const db = new Postgres(config);
const sequelize = getDb(config);

const clean = async () => {
  const opts = { truncate: true, cascade: true };
  await Transaction.destroy(opts);
  await Merchant.destroy(opts);
  await User.destroy(opts);
};

describe('persistence/pg/Postgres', () => {
  beforeEach(async () => {
    await clean();
  });

  after(async () => {
    await clean();
    await db.disconnect();
    await sequelize.close();
  });

  describe('getSummary', () => {
    it('Should return empty for no data', async () => {
      const from = new Date('2020-01-02T00:00:00');
      const to = new Date('2020-12-31T00:00:00');

      const summary = await db.getSummary('70c7df78-1168-4c45-a269-df9ad1efd1a3', from, to);
      expect(summary).to.be.an('array');
      expect(summary.length).to.equal(0);
    });

    it('Should definitely not allow SQL injection via userId', async () => {
      const from = new Date('2020-01-02T00:00:00');
      const to = new Date('2020-12-31T00:00:00');

      await rejects(db.getSummary('1; DELETE FROM merchants', from, to));
      await rejects(db.getSummary('1 OR 1=1', from, to));
    });

    it('Should correctly filter date', async () => {
      const date = new Date();
      const weekBefore = add(date, { days: -7 });
      const fortnightBefore = add(date, { days: -14 });

      const { id: merchantId, displayName } = await Merchant.create({ displayName: 'Lee House Chinese' });
      const [user1, user2, user3] = await User.bulkCreate([
        { firstName: 'Oliver', lastName: 'Noodle' },
        { firstName: 'Fred', lastName: 'Rice' },
        { firstName: 'Rich', lastName: 'Sauce' },
      ]);

      await Transaction.bulkCreate([
        {
          userId: user1.id, date, amount: 2187, merchantId,
        },
        {
          userId: user2.id, date, amount: 579, merchantId,
        },
        {
          userId: user2.id, date: weekBefore, amount: 186, merchantId,
        },
        {
          userId: user3.id, date: weekBefore, amount: 1288, merchantId,
        },
        {
          userId: user3.id, date: fortnightBefore, amount: 1390, merchantId,
        },
      ]);

      const from = add(date, { days: -1 });
      const [user1Summary] = await db.getSummary(user1.id, from, date);
      const [user2Summary] = await db.getSummary(user2.id, from, date);
      const user3Summary = await db.getSummary(user3.id, from, date);

      expect(user1Summary).to.include({
        displayName,
        totalSpent: '2187',
        percent: 100,
      });

      expect(user2Summary).to.include({
        displayName,
        // Should not include the old transaction.
        totalSpent: '579',
        percent: 0,
      });

      // Should be empty as no txs in that time.
      expect(user3Summary).to.deep.equal([]);
    });

    it('Should calculate correct values for users', async () => {
      const date = new Date();

      const { id: merchantId, displayName } = await Merchant.create({ displayName: 'Greggs' });
      const [user1, user2, user3] = await User.bulkCreate([
        { firstName: 'Alan', lastName: 'Spender' },
        { firstName: 'Steve', lastName: 'Tight' },
        { firstName: 'John', lastName: 'Middleton' },
      ]);

      await Transaction.bulkCreate([
        {
          userId: user1.id, date, amount: 1223, merchantId,
        },
        {
          userId: user2.id, date, amount: 144, merchantId,
        },
        {
          userId: user3.id, date, amount: 587, merchantId,
        },
      ]);

      const from = add(date, { days: -1 });
      const to = add(date, { days: 1 });

      const [user1Greggs] = await db.getSummary(user1.id, from, to);
      expect(user1Greggs).to.include({
        displayName,
        percent: 100,
        totalSpent: '1223',
      });

      const [user2Greggs] = await db.getSummary(user2.id, from, to);
      expect(user2Greggs).to.include({
        displayName,
        percent: 0,
        totalSpent: '144',
      });

      const [user3Greggs] = await db.getSummary(user3.id, from, to);
      expect(user3Greggs).to.include({
        displayName,
        percent: 50,
        totalSpent: '587',
      });
    });

    it('Should handle concurrent requests', async () => {
      const date = new Date();

      const { id: merchantId } = await Merchant.create({ displayName: 'Waitrose' });
      const { id: userId } = await User.create({
        firstName: 'Theodore',
        lastName: 'Whisker III',
      });

      await Transaction.bulkCreate([
        {
          userId, date, amount: 1223, merchantId,
        },
        {
          userId, date, amount: 144, merchantId,
        },
        {
          userId, date, amount: 587, merchantId,
        },
      ]);

      const from = add(date, { days: -1 });
      const to = add(date, { days: 1 });

      const [{ displayName, totalSpent, percent }] = await db.getSummary(userId, from, to);

      const responses = await Promise.all([
        db.getSummary(userId, from, to),
        db.getSummary(userId, from, to),
        db.getSummary(userId, from, to),
      ]);

      responses.forEach((r) => {
        expect(r[0]).to.include({ displayName, percent, totalSpent });
      });
    });
  });
});
