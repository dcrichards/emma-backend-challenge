import { Sequelize } from 'sequelize';
import defineTransactions, { Transaction } from '../../../src/persistence/pg/models/Transaction';
import defineMerchants, { Merchant } from '../../../src/persistence/pg/models/Merchant';
import defineUsers, { User } from '../../../src/persistence/pg/models/User';

interface Config {
  db: string;
  user: string;
  pass: string;
  host: string;
  port: number;
}

export const getDb = (config: Config): Sequelize => {
  const db = new Sequelize(config.db, config.user, config.pass, {
    host: config.host, port: config.port, dialect: 'postgres', logging: false,
  });

  defineUsers(db);
  defineMerchants(db);
  defineTransactions(db);

  return db;
};

export {
  Transaction,
  Merchant,
  User,
};
