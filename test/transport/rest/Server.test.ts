import supertest, { Response } from 'supertest';
import { expect } from 'chai';
import Server, { UserSpendingService } from '../../../src/transport/rest/Server';

const getServer = async (s?: UserSpendingService) => {
  const userSpendingService = s || {
    getSummary: () => Promise.resolve([]),
  };

  const server = new Server({
    userSpendingService,
    logger: {
      debug() {},
      info() {},
      error() {},
    },
  });

  await server.listen(0);
  const api = supertest(server);

  return { api, server };
};

describe('REST API', () => {
  describe('GET /alive', () => {
    it('Should successfully respond', async () => {
      const { api, server } = await getServer();
      await api.get('/alive').expect(200);
      server.close();
    });
  });

  describe('GET /nonexistent', () => {
    it('Should handle 404 correctly', async () => {
      const { api, server } = await getServer();
      await api.get('/nonexistent').expect(404).expect((res: Response) => {
        expect(res.body.error).to.be.a('string');
      });
      server.close();
    });
  });

  describe('GET /user/:userId/spending/summary', () => {
    it('Should reject missing and invalid to and from dates', async () => {
      const { api, server } = await getServer();

      const errorBody = (res: Response) => {
        expect(res.body.error).to.be.a('string');
      };

      const urls = [
        '/user/1/spending/summary',
        '/user/1/spending/summary?from=2020-01-01',
        '/user/1/spending/summary?to-2020-01-01',
        '/user/1/spending/summary?from=2020-01-08&to=31-12-2020',
        '/user/1/spending/summary?from=01/01/2020&to=2020-02-01',
        '/user/1/spending/summary?from=2020-08-01&to=2020-01-01',
      ];

      await Promise.all(urls.map(async (url) => api.get(url).expect(400).expect(errorBody)));
      server.close();
    });

    it('Should handle error getting service', async () => {
      const err = 'Private bad error';

      const userSpendingService = {
        getSummary() {
          throw new Error(err);
        },
      };

      const { api, server } = await getServer(userSpendingService);

      const url = '/user/1/spending/summary?from=2020-01-01&to=2020-12-01';
      await api.get(url).expect(500).expect((res: Response) => {
        expect(res.body.error).to.not.equal(err);
      });
      server.close();
    });

    it('Should respond with correct schema', async () => {
      const userSpendingService = {
        getSummary() {
          return Promise.resolve([
            { displayName: 'Greggs', totalSpent: '185000', percent: 22.66667 },
            { displayName: 'Trago Mills', totalSpent: '28890', percent: 98.9 },
            { displayName: 'Pret', totalSpent: '185', percent: 8.55 },
          ]);
        },
      };

      const { api, server } = await getServer(userSpendingService);

      const url = '/user/79018d08-1fa2-44e1-9b8f-80a40c049261/spending/summary?from=2020-01-01&to=2020-12-01';
      await api.get(url).expect(200).expect((res: Response) => {
        const merchants = [
          { merchant: 'Greggs', percentile: 22.67 },
          { merchant: 'Trago Mills', percentile: 98.90 },
          { merchant: 'Pret', percentile: 8.55 },
        ];
        expect(res.body.merchants).to.have.deep.members(merchants);
      });
      server.close();
    });
  });
});
