# emma-backend-challenge

> Emma backend engineering challenge.

## Development

### Configuration

Configuration is handled by a `.env` file and [dotenv](https://www.npmjs.com/package/dotenv). See `.env.example` for expected variables.

```bash
cp .env.example .env
$EDITOR .env
```

The `.env` file is added to `.gitignore` to prevent secrets being compromised.

### Setup

#### Docker

1. Firstly, ensure your `POSTGRES_PASSWORD` is set in your `.env` file, as this value is shared by the Docker container which bootstraps Postgres.

2. Run the following:

```bash
docker-compose build
docker-compose run --rm emma yarn run db:migrate
# Optional - if you need some test data to work with.
docker-compose run --rm emma db:seed:dev
docker-compose up
```

The API should be running on `localhost:8000`

#### No Docker

1. Grab yourself a [Postgres database](https://www.postgresql.org/download/) and set it up as instructed.

2. Create dev and test databases. By default Postgres ships with a `postgres` database. You may either use and simply create `postgres_test` or create your own names.

```bash
psql -h host -p port -U user -d db_name
```
```sql
CREATE DATABASE postgres_test;
```

3. Ensure your environment variables are correct and match the database in your `.env` file.

4. Run the following:

```bash
yarn
yarn run db:migrate
# Optional - if you need some test data to work with.
yarn run db:seed:dev
yarn start
```

## Code Quality

Code quality is checked by [Eslint](https://eslint.org/) and follows the `airbnb-typescript/base` standard.


```bash
yarn run lint
```

## Testing

Testing is carried out with [Mocha](https://mochajs.org/) and [Chai](https://www.chaijs.com/). We currently use [supertest](https://github.com/visionmedia/supertest) for simple API testing.

Database tests are run against a real database, so ensure your test database is migrated the first time you run the tests.

```bash
# Note the env here to ensure the correct DB is migrated.
NODE_ENV=test yarn run db:migrate
```

The tests can then be run with and without coverage.

```bash
yarn test
# With coverage
yarn run test:coverage
```

## Database

[Sequelize](https://sequelize.org/master/index.html) is used as an ORM and migration manager.

### Configuration

Running any `sequelize-cli` commands will use the configuration in `config/sequelize.js`. The values inherit from the provided env vars in `.env`, with the addition of different configurations per `NODE_ENV`. Take care to check this before running any commands against the DB.

### Migrations

These are stored in `scripts/migrations` and can be auto-generated. It is recommended to use the auto-generator to ensure naming uniqueness and convention. For example:


```bash
npx sequelize-cli migration:generate --name update-user
```

## REST API

### `GET /alive`

Used for automated/manual healthchecks.

#### Response

```json
{ "alive": true }
```

### `GET /user/:id/spending/summary`

Get the spending summary for a given user.


#### Params

* `from` - Show summary from (and including) this date. Formatted in `YYYY-MM-DD`. Required.
* `to` - Show summary to (and including) this date. Formatted in `YYYY-MM-DD`. Required.

#### Response

```json
{
  "merchants": [
    {
      "merchant": "Greggs",
      "percentile": 98.8
    },
    {
      "merchant": "Seven Eleven",
      "percentile": 12.5
    }
  ]
}
```

